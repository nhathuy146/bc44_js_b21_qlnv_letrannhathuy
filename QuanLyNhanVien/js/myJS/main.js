var dsnv = [];
// Lấy json khi load trang
var dataJson = localStorage.getItem("DSNV_LOCAL");
// Conver json sang array
if (dataJson != null) {
  var dataArr = JSON.parse(dataJson);
  for (var i = 0; i < dataArr.length; i++) {
    var item = dataArr[i];
    var nv = new NhanVien(
      item.taiKhoan,
      item.hoTen,
      item.email,
      item.matKhau,
      item.ngayLam,
      item.luongCoBan,
      item.chucVu,
      item.gioLam
    );
    dsnv.push(nv);
  }
  renderDSNV(dsnv);
}
// Ẩn tài khoản khi chỉnh sửa
document.querySelector("#btnThem").addEventListener("click", function () {
  document.getElementById("tknv").disabled = false;
  document.getElementById("btnThemNV").disabled = false;

  // reset form
  document.querySelector("#myModal form").reset();
});

// Thêm nhân vien
document.querySelector("#btnThemNV").addEventListener("click", function () {
  var nv = layThongTin();
  var validateInput = validate(nv, true);

  if (validateInput) {
    dsnv.push(nv);
    // convert sang json
    var dataJson = JSON.stringify(dsnv);
    //Lưu json
    localStorage.setItem("DSNV_LOCAL", dataJson);
    renderDSNV(dsnv);
  }
});

// Xóa nhân viên
function xoaNV(id) {
  var viTri = -1;
  for (var i = 0; i < dsnv.length; i++) {
    var nv = dsnv[i];
    if (nv.taiKhoan == id) {
      viTri = i;
    }
  }
  dsnv.splice(viTri, 1);
  renderDSNV(dsnv);
}

// Sửa NV
function suaNV(id) {
  document.getElementById("tknv").disabled = true;
  document.getElementById("btnThemNV").disabled = true;

  var viTri = dsnv.findIndex(function (item) {
    return item.taiKhoan == id;
  });

  if (viTri != -1) {
    showThongTinLenForm(dsnv[viTri]);
  }
}

// Cập nhật Nhân viên
document.querySelector("#btnCapNhat").addEventListener("click", function () {
  var nv = layThongTin();
  var viTri = dsnv.findIndex(function (item) {
    return item.taiKhoan == nv.taiKhoan;
  });
  var validateInput = validate(nv, false);
  if (viTri != -1 && validateInput) {
    dsnv[viTri] = nv;
    renderDSNV(dsnv);
    // Đóng modal khi nhập đúng điều kiện
    $("#myModal").modal("hide");
  }
  console.log(`🚀 ~ capNhatNhanVien ~ dsnv:`, dsnv);
});

// Tìm Kiếm
document.getElementById("btnTimNV").addEventListener("click", function () {
  var timKiem = document.getElementById("searchName").value.trim();
  document.getElementById("searchName").value = "";
  var ketQua = dsnv.filter(function (value) {
    return value.xepLoai() === timKiem;
  });
  renderDSNV(ketQua);
});

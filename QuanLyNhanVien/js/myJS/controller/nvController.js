function layThongTin() {
  var taiKhoan = document.querySelector("#tknv").value;
  var hoTen = document.querySelector("#name").value;
  var email = document.querySelector("#email").value;
  var matKhau = document.querySelector("#password").value;
  var ngayLam = document.querySelector("#datepicker").value;
  var luongCoBan = document.querySelector("#luongCB").value * 1;
  var chucVu = document.querySelector("#chucVu").value;
  var gioLam = document.querySelector("#gioLam").value * 1;

  var nv = new NhanVien(
    taiKhoan,
    hoTen,
    email,
    matKhau,
    ngayLam,
    luongCoBan,
    chucVu,
    gioLam
  );
  return nv;
}

function renderDSNV(nvArr) {
  var contentHTML = "";
  for (var i = 0; i < nvArr.length; i++) {
    var nv = nvArr[i];
    var contentTr = `<tr>
                    <td>${nv.taiKhoan}</td>
                    <td>${nv.hoTen}</td>
                    <td>${nv.email}</td>
                    <td>${nv.ngayLam}</td>
                    <td>${nv.chucVu}</td>
                    <td>${nv.tongLuong()}</td>
                    <td>${nv.xepLoai()}</td>
                    <td>
                    <button onclick="xoaNV(${
                      nv.taiKhoan
                    })" class="btn btn-danger">Xóa</button>            
                    <button onclick="suaNV(${
                      nv.taiKhoan
                    })" class="btn btn-secondary" data-toggle="modal"
									data-target="#myModal">Sửa</button>
                </td>
        </tr>`;
    // Nối chuỗi thẻ td với nhau
    contentHTML += contentTr;
  }
  document.querySelector("#tableDanhSach").innerHTML = contentHTML;
}

function showThongTinLenForm(nv) {
  document.querySelector("#tknv").value = nv.taiKhoan;
  document.querySelector("#name").value = nv.hoTen;
  document.querySelector("#email").value = nv.email;
  document.querySelector("#password").value = nv.matKhau;
  document.querySelector("#datepicker").value = nv.ngayLam;
  document.querySelector("#luongCB").value = nv.luongCoBan;
  document.querySelector("#chucVu").value = nv.chucVu;
  document.querySelector("#gioLam").value = nv.gioLam;
}

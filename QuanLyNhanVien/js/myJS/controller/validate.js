var anBox = function (spanErr) {
  document.querySelector("#" + spanErr).style.display = "none";
};
var hienBox = function (spanErr) {
  document.querySelector("#" + spanErr).style.display = "inline-block";
};

var showMessage = function (id, message) {
  document.getElementById(id).innerHTML = `<strong>${message}</strong>`;
};

// 1.Validate Tài Khoản
// 1.1 Kiểm tra trùng
var kiemTraTrung = function (taiKhoan, dsnv) {
  var index = dsnv.findIndex(function (item) {
    return taiKhoan == item.taiKhoan;
  });

  if (index === -1) {
    anBox("tbTKNV");
    showMessage("tbTKNV", "");
    return true;
  } else {
    hienBox("tbTKNV");
    showMessage("tbTKNV", "Số tài khoản đã tồn tại");
    return false;
  }
};

//1.2 kiểm tra tài khoản là số và có 4 đến 6 chữ số
var kiemTraTaiKhoanSo = function (taiKhoan) {
  const re = /^\d{4,6}$/;
  if (re.test(taiKhoan)) {
    anBox("tbTKNV");
    showMessage("tbTKNV", "");
    return true;
  } else {
    hienBox("tbTKNV");
    showMessage("tbTKNV", "Tài khoản phải là số và có từ 4 đến 6 số");
    return false;
  }
};

// Kiểm tra rỗng
var kiemTraRong = function (idErr, value) {
  if (value.length == 0) {
    hienBox(idErr);
    showMessage(idErr, "Không được để trống");
    return false;
  } else {
    anBox(idErr);
    showMessage(idErr, "");
    return true;
  }
};

// Kiểm tra định dạng phải là số
function phaiLaSo(idErr, value) {
  const regexSo = /^\d+$/;
  if (regexSo.test(value)) {
    anBox(idErr);
    showMessage(idErr, "");
    return true;
  } else {
    hienBox(idErr);
    showMessage(idErr, "Không nhập chữ vào ô này");
    return false;
  }
}
// Kiểm tra rỗng dành cho Number
var kiemTraSoRong = function (idErr, value) {
  if (Number(value) === 0) {
    hienBox(idErr);
    showMessage(idErr, "Không được để trống");
    return false;
  } else {
    anBox(idErr);
    showMessage(idErr, "");
    return true;
  }
};
// 2. Kiểm tra tên nhân viên là chữ tiếng việt
function kiemTraTenNV(hoTen) {
  const re = /[\p{Lu}\p{Ll}\p{M}\s]+/gu;
  if (re.test(hoTen)) {
    anBox("tbTen");
    showMessage("tbTen", "");
    return true;
  } else {
    hienBox("tbTen");
    showMessage("tbTen", "Tên nhập phải là chữ");
    return false;
  }
}

// 3.Kiểm tra email
function kiemTraEmail(email) {
  const re =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  if (re.test(email)) {
    anBox("tbEmail");
    showMessage("tbEmail", "");
    return true;
  } else {
    hienBox("tbEmail");
    showMessage("tbEmail", "vui lòng nhập email chính xác");
    return false;
  }
}
// 4.Kiểm tra mật khẩu
function kiemTraMatKhau(matKhau) {
  const re =
    /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*()_+\-=[\]{};':"\\|,.<>/?]).{6,10}$/;
  if (re.test(matKhau)) {
    anBox("tbMatKhau");
    showMessage("tbMatKhau", "");
    return true;
  } else {
    hienBox("tbMatKhau");
    showMessage(
      "tbMatKhau",
      "vui lòng nhập mật khẩu có 6-10 ký tự và chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt"
    );
    return false;
  }
}
// 5. Kiểm tra ngày
// Hàm này dùng kiểm tra ngày tháng năm trả về là chuỗi
function kiemTraRongNgayThangNam(idErr, ngaylam) {
  // Kiểm tra rỗng cho ngày tháng năm
  if (!ngaylam || ngaylam.trim().length === 0) {
    hienBox(idErr);
    showMessage(idErr, "Không được để trống");
    return false;
  } else {
    // Kiểm tra định dạng ngày tháng năm nếu không rỗng
    anBox(idErr);
    showMessage(idErr, "");
    const [ngay, thang, nam] = ngaylam.split("/");
    const isValidDate = !isNaN(Date.parse(`${thang}/${ngay}/${nam}`));
    if (!isValidDate) {
      hienBox(idErr);
      showMessage(idErr, "Ngày tháng năm không hợp lệ");
      return false;
    }
    return true;
  }
}

// 6. Kiểm tra lương
function kiemTraLuong(idErr, luongCoBan) {
  // Kiểm tra định dạng là số
  if (!isNaN(luongCoBan)) {
    anBox(idErr);
    showMessage(idErr, "");
    // Kiểm tra nhập số lương chính xác
    if (1.0e6 <= luongCoBan && luongCoBan <= 20.0e6) {
      anBox(idErr);
      showMessage(idErr, "");
      return true;
    } else {
      hienBox(idErr);
      showMessage(idErr, "Nhập đúng số lương từ 1.000.000 đên 20.000.000");
      return false;
    }
  } else {
    hienBox(idErr);
    showMessage(idErr, "Vui lòng nhập số chính xác");
    return false;
  }
}

// 7. Kiểm tra chức vụ
function kiemTraChucVu(idErr, chucVu) {
  if (chucVu == "Chọn chức vụ") {
    hienBox(idErr);
    showMessage(idErr, "Vui lòng chọn chức vụ chính xác");
    return false;
  } else {
    anBox(idErr);
    showMessage(idErr, "");
    return true;
  }
}

// 8. Kiểm tra giờ làm
function kiemTraGioLam(idErr, gioLam) {
  if (80 <= gioLam && gioLam <= 200) {
    anBox(idErr);
    showMessage(idErr, "");
    return true;
  } else {
    hienBox(idErr);
    showMessage(idErr, "Nhập giờ làm trong khoảng 80-200");
    return false;
  }
}

function validate(nv, checkTrung = true) {
  // Validate Tài Khoản
  var isValid =
    kiemTraRong("tbTKNV", nv.taiKhoan) && kiemTraTaiKhoanSo(nv.taiKhoan);
  // Không báo trùng khi cập nhật
  if (checkTrung) {
    isValid = isValid && kiemTraTrung(nv.taiKhoan, dsnv);
  }

  // Validate Tên nhân viên
  isValid = isValid & kiemTraRong("tbTen", nv.hoTen) && kiemTraTenNV(nv.hoTen);
  // Validate Email
  isValid =
    isValid & kiemTraRong("tbEmail", nv.email) && kiemTraEmail(nv.email);
  // Validate mật khẩu
  isValid =
    isValid & kiemTraRong("tbMatKhau", nv.matKhau) &&
    kiemTraMatKhau(nv.matKhau);
  // Validate ngày làm
  isValid = isValid & kiemTraRongNgayThangNam("tbNgay", nv.ngayLam);
  // Validate Lương Cơ Bản
  isValid =
    isValid & kiemTraSoRong("tbLuongCB", nv.luongCoBan) &&
    phaiLaSo("tbLuongCB", nv.luongCoBan) &&
    kiemTraLuong("tbLuongCB", nv.luongCoBan);
  // Validate Chức vụ
  isValid = isValid & kiemTraChucVu("tbChucVu", nv.chucVu);
  // Validate Giờ làm
  isValid =
    isValid & kiemTraSoRong("tbGioLam", nv.gioLam) &&
    phaiLaSo("tbGioLam", nv.gioLam) &&
    kiemTraGioLam("tbGioLam", nv.gioLam);
  return isValid;
}
